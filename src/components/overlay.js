/**
 * Copyright (c) 2017 Dmitry Soloma(flyingmeatsoul@gmail.com).
 * https://bitbucket.org/dmitry_soloma/pain
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

/**
 * Overlay
 * @param params
 * @constructor
 */
function OverlayComponent(params) {
    Utils.object.extends(OverlayComponent, Component, this, params);
    this.cssClass = function () {
        return {
            name: 'basic-overlay',
            rules: 'position: fixed; overflow-y: auto;' +
            ' display: none; align-items: center; justify-content: ' +
            'center; width: 100%; height: 100%; top: 0; left: 0; ' +
            'right: 0; bottom: 0; background-color: rgba(0, 0, 0, 0.5); ' +
            'z-index: 2;'
        }
    };
    this.overlay = function () {
        return _overlay;
    };
    this.show = function () {
        _overlay.style.display = 'flex';
        return this;
    };
    this.hide = function () {
        _overlay.style.display = 'none';
        return this;
    };
    this.render = function (content) {
        _overlay.appendChild(content);
        return this;
    };
    this.reset = function () {
        _overlay.innerHTML = '';
        return this;
    };
    /**
     * Main
     */
    var _overlay = new Element({node: 'div', className: 'basic-overlay'});
    this.getCss().createClass(this.cssClass().name, this.cssClass().rules);
    params.dom.append(_overlay);
}