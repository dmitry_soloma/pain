/**
 * Copyright (c) 2017 Dmitry Soloma(flyingmeatsoul@gmail.com).
 * https://bitbucket.org/dmitry_soloma/pain
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

/**
 * Message Type
 * @type {{ERROR: string, SUCCESS: string, WARNING: string, NOTICE: string}}
 */
var MessageType = {
    ERROR: 'error',
    SUCCESS: 'success',
    WARNING: 'warning',
    NOTICE: 'notice',
};

/**
 * User message types
 * @constructor
 */
var UserMessageTypes = {
    'success': 'type-success',
    'error': 'type-error',
    'warning': 'type-warning',
    'notice': 'type-notice'
};

/**
 * Css Styles
 */
var cssStyles = {
    'message-item': 'display: block; opacity: 0; padding-left: 0; padding-right: 0; min-width: 200px; width: 100%; height: 20px; ' +
    'z-index: 10; color: #ffffff; padding-top: 1px; border-radius: 3px; text-align: center; position: absolute;',
    'type-error': 'border: 1px solid #ffabab; background-color: rgba(255, 35, 0, 0.62); box-shadow: 0 5px 10px 0 rgba(255, 124, 124, 0.3); ' +
    'animation-name: simple; animation-duration: 6.5s; animation-fill-mode: forwards;',
    'type-warning': 'background-color: rgba(255, 167, 0, 0.7); box-shadow: 0 5px 10px 0 rgba(255, 210, 124, 0.44); border: 1px solid #ffee00; ' +
    'animation-name: simple; animation-duration: 6.5s; animation-fill-mode: forwards;',
    'type-notice': 'background-color: rgba(156, 156, 156, 0.7); box-shadow: 0 5px 10px 0 rgba(156, 156, 156, 0.44); border: 1px solid #c5c5c5; ' +
    'animation-name: simple; animation-duration: 6.5s; animation-fill-mode: forwards;',
    'type-success': 'background-color: rgba(31, 162, 0, 0.7); box-shadow: 0 5px 10px 0 rgba(0, 134, 5, 0.44);b order: 1px solid #00af17; ' +
    'animation-name: simple; animation-duration: 6.5s; animation-fill-mode: forwards;',
};

/**
 * User Animation
 */
var UserMessageAnimation = {
    'simple': '0% {opacity: 0;} 10% {opacity: 1;} 85% {opacity: 1;} 100% {opacity: 0;}'
};

/**
 * User Message Component
 * @param params
 * @constructor
 */
function UserMessageComponent(params) {
    Utils.object.extends(UserMessageComponent, Component, this, params);
    this.yOffsetIncrement = 26;
    this.startPositionX = 100;
    this.startPositionY = 0;
    this.hideTimeout = 6500;
    this.getMessages = function (id) {
        if (typeof id !== 'undefined') {
            return this.getItems().getItemById(id);
        }
        return this.getItems();
    };
    this.add = function (name, text, type) {
        if (this.getItems().getItemsByAttributes({name: name, text: text}).size()) {
            throw 'User message with same text and name already exists';
        }
        var item = new Item({id: _defaultId++, name: name, text: text, type: type});
        this.getItems().addItem(item);
        return this;
    };
    this.clear = function () {
        this.getItems().clear();
        return this;
    };
    this.getMessageClass = function (type) {
        if (!UserMessageTypes.hasOwnProperty(type)) {
            throw 'Type is not defined';
        }
        var messageClass = UserMessageTypes[type];
        if (!cssStyles.hasOwnProperty(messageClass)) {
            throw 'Class is not defined';
        }
        return messageClass;
    };
    this.removeMessage = function (id, userMessage) {
        var self = this;
        setTimeout(function () {
            self.getDom().removeChild(userMessage);
            self.getItems().removeItemById(id);
            _yOffset -= self.yOffsetIncrement;
            if (_yOffset < 0) {
                _yOffset = 0;
            }
        }, this.hideTimeout);
    };
    this.render = function (type) {
        if (!this.getItems().size()) {
            return this;
        }
        var self = this;
        this.getItems().eachItem(function (item, id) {
            if (typeof type !== 'undefined') {
                if (type !== item.getData('type')) {
                    return false;
                }
            }
            var yPos = self.startPositionY + _yOffset + self.getDom().scrollTop;
            _yOffset += self.yOffsetIncrement;
            var userMessage = new Element({
                node: 'div',
                innerHTML: item.getData('text'),
                className: 'message-item ' + self.getMessageClass(item.getData('type'))
            });
            userMessage.style.top = yPos + 'px';
            self.getDom().appendChild(userMessage);
            self.removeMessage(id, userMessage);
        });
        return this;
    };
    /**
     * Main
     */
    var _defaultId = 1;
    var _yOffset = 0;
    for (var name in cssStyles) {
        this.getCss().createClass(name, cssStyles[name]);
    }
    for (var name in UserMessageAnimation) {
        this.getCss().createKeyframes(name, UserMessageAnimation[name]);
    }
}