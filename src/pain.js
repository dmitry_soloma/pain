/**
 * Copyright (c) 2017 Dmitry Soloma(flyingmeatsoul@gmail.com).
 * https://bitbucket.org/dmitry_soloma/pain
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

/**
 * Data Object
 * @param data
 * @constructor
 */
function DataObject(data) {
    var _data = {};
    var _separator = '.';
    var _setData = function (data) {
        for (var key in data) {
            _prepareKey(key, data[key]);
        }
    };
    var _prepareKey = function (key, value) {
        var __data = _data;
        if (value !== null && typeof value === 'object') {
            var _dataObject = new DataObject(value);
            value = _dataObject.getData();
        }
        if (key.indexOf(_separator) !== -1) {
            var index;
            var keys = key.split(_separator);
            for (var i = 0; i < keys.length - 1; i++) {
                index = keys[i];
                if (!__data.hasOwnProperty(index) || typeof __data[index] !== 'object') {
                    __data[index] = {};
                }
                __data = __data[index];
            }
            index = keys[keys.length - 1];
            __data[index] = value;
        } else {
            __data[key] = value;
        }
        return this;
    };
    this.setData = function (key, value) {
        if (typeof key === 'object') {
            this.unsetData();
            _setData(key);
        } else {
            _prepareKey(key, value);
        }
        return this;
    };
    this.getSeparator = function () {
        return _separator;
    };
    this.setSeparator = function (separator) {
        _separator = separator;
        return this;
    };
    this.getData = function (key) {
        if (typeof key !== 'undefined') {
            var __data = _data;
            if (key.indexOf(_separator) !== -1) {
                var keys = key.split(_separator);
                for (var i = 0; i < keys.length; i++) {
                    var index = keys[i];
                    if (__data.hasOwnProperty(index)) {
                        __data = __data[index];
                    } else {
                        return null;
                    }
                }
                if (typeof __data === 'object') {
                    return new DataObject(__data);
                } else {
                    return __data;
                }
            } else {
                if (__data.hasOwnProperty(key)) {
                    if (typeof __data[key] === 'object') {
                        return new DataObject(__data[key]);
                    } else {
                        return __data[key]
                    }
                } else {
                    return null;
                }
            }
        }
        return _data;
    };
    this.unsetData = function (key) {
        if (typeof key === 'undefined') {
            _data = {};
        } else {
            var __data = _data;
            if (key.indexOf(_separator) !== -1) {
                var index;
                var keys = key.split(_separator);
                for (var i = 0; i < keys.length - 1; i++) {
                    index = keys[i];
                    if (__data.hasOwnProperty(index)) {
                        __data = __data[index];
                    } else {
                        throw 'Unknown path';
                    }
                }
                index = keys[keys.length - 1];
                if (__data.hasOwnProperty(index)) {
                    delete __data[index];
                }
            } else {
                if (__data.hasOwnProperty(key)) {
                    delete __data[key];
                }
            }
        }
        return this;
    };
    this.hasData = function (key) {
        var __data = _data;
        if (key.indexOf(_separator) !== -1) {
            var index;
            var keys = key.split(_separator);
            for (var i = 0; i < keys.length; i++) {
                index = keys[i];
                if (__data.hasOwnProperty(index)) {
                    __data = __data[index];
                } else {
                    return false;
                }
            }
            return true;
        } else {
            return __data.hasOwnProperty(key);
        }
    };
    if (typeof data === 'object') {
        this.setData(data);
    }
}

/**
 * Element
 * @param params
 * @returns {*}
 * @constructor
 */
function Element(params) {
    var _element;
    if (typeof params === 'undefined') {
        throw "Params is required";
    }
    if (typeof params.node === 'undefined') {
        throw "Element node is undefined";
    }
    _element = document.createElement(params.node);
    for (var property in params) {
        if (property === 'data') {
            for (var dataProp in params[property]) {
                _element.dataset[dataProp] = params[property][dataProp];
            }
        } else {
            _element[property] = params[property];
        }
    }
    return _element;
}

/**
 * TestCase
 * @constructor
 */
function TestCase() {
    var _echo = function (message) {
        console.log(message);
    };
    this.assert = function (condition, message) {
        if (!condition) {
            throw "Assert failed" + (typeof message !== "undefined" ? ": " + message : "");
        }
        _echo('Assert success');
        return true;
    };
}

/**
 * Item Object
 * @param params
 * @constructor
 */
function Item(params) {
    var _data;
    this.getData = function (key) {
        if (typeof key === 'undefined') {
            return _data.getData();
        }
        return _data.getData(key);
    };
    this.setData = function (params) {
        if (!params || typeof params['id'] === 'undefined') {
            throw 'Params must have an id attribute';
        }
        _data = new DataObject(params);
        return this;
    };
    this.getId = function () {
        return this.getData('id');
    };
    this.clear = function () {
        _data = new DataObject({id: null});
        return this;
    };
    this.setData(params);
}

/**
 * Item collection
 * @param params
 * @constructor
 */
function ItemCollection(params) {
    var _collection = {};
    this.clear = function () {
        _collection = {};
        return this;
    };
    this.addItem = function (item) {
        if (typeof item !== 'object') {
            throw "Item has unsupported type";
        }
        var id = item.getId();
        _collection[id] = item;
        return this;
    };
    this.eachItem = function (callback) {
        for (var id in _collection) {
            callback(_collection[id], id);
        }
        return this;
    };
    this.removeItemById = function (id) {
        if (_collection.hasOwnProperty(id)) {
            delete _collection[id];
        }
        return this;
    };
    this.getCollection = function () {
        return _collection;
    };
    this.getItemById = function (id) {
        if (_collection.hasOwnProperty(id)) {
            return _collection[id];
        }
        return null;
    };
    this.getItemByAttributeValue = function (attribute, value) {
        for (var id in _collection) {
            if (_collection[id].getData(attribute) === value) {
                return _collection[id];
            }
        }
        return null;
    };
    this.getItemsByAttributes = function (attributes) {
        var result = new ItemCollection();
        for (var id in _collection) {
            var match = true;
            for (var attribute in attributes) {
                if (_collection[id].getData(attribute) !== attributes[attribute]) {
                    match = false;
                    break;
                }
            }
            if (match) {
                result.addItem(_collection[id]);
            }
        }
        return result;
    };
    this.size = function () {
        var _size = 0;
        for (var id in _collection) {
            _size++;
        }
        return _size;
    };
    this.getFirstItem = function () {
        var _index = Object.keys(_collection)[0];
        if (_index) {
            return _collection[_index];
        }
        return null;
    };
    this.getLastItem = function () {
        var _item = null;
        var _keys = Object.keys(_collection);
        var _index = _keys[_keys.length - 1];
        if (_index) {
            _item = _collection[_index];
        }
        return _item;
    };
    if (params === null) {
        throw 'Null value passed as params';
    }
    for (var index in params) {
        var item = new Item(params[index]);
        this.addItem(item);
    }
}

/**
 * Collback Collection
 * @param params
 * @constructor
 */
function CallbackCollection(params) {
    _collection = {};
    this.all = function () {
        return _collection;
    };
    this.getByName = function (name) {
        if (_collection.hasOwnProperty(name)) {
            return _collection[name];
        }
        return null;
    };
    this.add = function (name, callback) {
        if (typeof callback !== 'function') {
            throw 'Callback is not a function';
        }
        _collection[name] = callback;
        return this;
    };
    this.remove = function (name) {
        if (_collection.hasOwnProperty(name)) {
            delete _collection[name];
        } else {
            throw 'Callback not found';
        }
        return this;
    };
    this.each = function (callback) {
        for (var id in _collection) {
            callback(_collection[id], id);
        }
        return this;
    };
    if (params === null) {
        throw 'Null value passed as params';
    }
    for (var index in params) {
        this.add(index, params[index]);
    }
}

/**
 * Style component
 * @param params
 * @constructor
 */
function Css(params) {
    var _style = document.getElementsByTagName('style');
    var _head = document.getElementsByTagName('head')[0];
    if (_style.length) {
        _style = _style[0];
    } else {
        _style = new Element({
            node: 'style',
            type: 'text/css'
        });
        _head.appendChild(_style);
    }
    var _getClassName = function (name) {
        return name = '.' + name;
    };
    var _getKeyframesName = function (name) {
        return name = '@keyframes ' + name;
    };
    this.addRule = function (name, rules) {
        if (!(_style.sheet || {}).insertRule) {
            (_style.styleSheet || _style.sheet).addRule(name, rules);
        } else {
            _style.sheet.insertRule(name + "{" + rules + "}", 0);
        }
        return this;
    };
    this.removeRule = function (name) {
        for (index in _style.sheet.rules) {
            if (name === _style.sheet.rules[index]['selectorText']) {
                if (!(_style.sheet || {}).deleteRule) {
                    (_style.styleSheet || _style.sheet).removeRule(index);
                } else {
                    _style.sheet.deleteRule(index);
                }
            }
        }
        return this;
    };
    this.changeRule = function (name, rules) {
        var _rule;
        for (index in _style.sheet.rules) {
            if (name === _style.sheet.rules[index]['selectorText']) {
                _rule = _style.sheet.rules[index];
                _rule.style.cssText = rules;
            }
        }
        if (typeof _rule === 'undefined') {
            throw 'Rule not found';
        }
        return this;
    };
    this.createClass = function (name, rules) {
        return this.addRule(_getClassName(name), rules);
    };
    this.removeClass = function (name) {
        return this.removeRule(_getClassName(name));
    };
    this.changeClass = function (name, rules) {
        return this.changeRule(_getClassName(name), rules);
    };
    this.createKeyframes = function (name, rules) {
        return this.addRule(_getKeyframesName(name), rules);
    };
    this.removeKeyframes = function (name) {
        return this.removeRule(_getKeyframesName(name));
    };
    this.changeKeyframes = function (name, rules) {
        return this.changeRule(_getKeyframesName(name), rules);
    };
}

/**
 * Component
 * @param params {
 *      request: new XMLHttpRequest(),
 *      dom: document.getElementsByClassName('test')[0],
 *      items: [
 *          {id: 1, name: 'one'},
 *          {id: 2, name: 'two'},
 *          {id: 3, name: 'three'}
 *      ],
 *      css: null,
 *      callbacks: {
 *          'test_action': function () {
 *              return 'its works';
 *          },
 *          'on_button_click': function () {
 *              return 'clicked';
 *          }
 *      }
 * }
 * @constructor
 */
function Component(params) {
    if (typeof params !== 'object') {
        throw "Params must be an object";
    }
    this.initRequest = function (request) {
        if (typeof request !== 'undefined') {
            return request;
        }
        return new XMLHttpRequest();
    };
    this.initDom = function (dom) {
        if (typeof dom !== 'object') {
            throw 'Property "dom" must be defined and must be an object';
        }
        return dom;
    };
    this.initItems = function (items) {
        if (typeof items !== 'undefined') {
            return new ItemCollection(items);
        }
        return new ItemCollection();
    };
    this.initCss = function (css) {
        return new Css(css);
    };
    this.initCallbacks = function (callbacks) {
        if (typeof callbacks !== 'undefined') {
            return new CallbackCollection(callbacks);
        }
        return new CallbackCollection();
    };
    this.getRequest = function () {
        return _xhr;
    };
    this.getDom = function () {
        return _dom;
    };
    this.getItems = function () {
        return _items;
    };
    this.getCss = function () {
        return _css;
    };
    this.getCallbacks = function () {
        return _callbacks;
    };
    var _xhr = this.initRequest(params.request);
    var _dom = this.initDom(params.dom);
    var _items = this.initItems(params.items);
    var _css = this.initCss(params.css);
    var _callbacks = this.initCallbacks(params.callbacks);
}

/**
 * Utils
 * @type {{string, object}}
 */
var Utils = function () {
    return {
        string: {
            pad: function (d, n, s) {
                var pow = 1;
                var result = d.toString();
                s = typeof s === 'undefined' ? '0' : s;
                for (var i = 1; i < n; i++) {
                    pow *= 10;
                    if (d < pow) {
                        result = s + result;
                    }
                }
                return result;
            },
            uriParams: function (obj, s) {
                s = typeof s === 'undefined' ? '&' : s;
                return '?' + Object.keys(obj).reduce(function (a, k) {
                        a.push(k + '=' + encodeURIComponent(obj[k]));
                        return a
                    }, []).join(s);
            }    
        },
        object: {
            extends: function (target, source, instance, params) {
                target.prototype = Object.create(source.prototype);
                target.prototype.constructor = source;
                source.call(instance, params);
            } 
        }
    };
}();

/**
 * Pills
 * @constructor
 */
function Pills() {

}
